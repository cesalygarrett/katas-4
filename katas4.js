const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function displayResults (functionName, input){
    const header = document.createElement("h2");
    header.textContent = functionName;

    const newElement = document.createElement("div");
    newElement.textContent = input;

    document.body.appendChild(header);
    document.body.appendChild(newElement);
}

function katas1(){
    displayResults('Katas 1', JSON.stringify(gotCitiesCSV.split(',')));
    return gotCitiesCSV
}

katas1()

function katas2(){
   displayResults('Katas 2', JSON.stringify(bestThing.split(' ')));
   return bestThing
}

katas2()
// Split why doesn't a semicolon work?
function katas3(){
    let CSVarray = gotCitiesCSV.split(',')
    displayResults('Katas 3', JSON.stringify(CSVarray.join('; ')))
    return CSVarray
}
katas3()
// Do I need spaces? How do I get spaces?
function katas4(){
    displayResults('Katas 4', JSON.stringify(lotrCitiesArray.toString()))
    return lotrCitiesArray
}

katas4()

function katas5(){
    displayResults('Katas 5', JSON.stringify(lotrCitiesArray.slice(0, 5)))
    return lotrCitiesArray
}

katas5()

function katas6(){
    displayResults('Katas 6', JSON.stringify(lotrCitiesArray.slice(3)))
    return lotrCitiesArray
}

katas6()

function katas7(){
    displayResults('Katas 7', JSON.stringify(lotrCitiesArray.slice(2, 5)))
    return lotrCitiesArray
}

katas7()

function katas8(){
    lotrCitiesArray.splice(2, 1);
    displayResults('Katas 8', JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray
}

katas8()
// Could I use pop() twice? 
function katas9(){
    lotrCitiesArray.splice(5, 2)
    displayResults('Katas 9', JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray
}

katas9()

function katas10(){
    lotrCitiesArray.splice(2, 0, 'Rohan')
    displayResults('Katas 10', JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray 
}

katas10()

function katas11(){
    lotrCitiesArray.splice(5, 1, 'Deadest Marshes')
    displayResults('Katas 11', JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray
}

katas11()

function katas12(){
    displayResults('Katas 12', JSON.stringify(bestThing.slice(0, 14)));
    return bestThing
}

katas12()

function katas13(){
    displayResults('Katas 13', JSON.stringify(bestThing.slice(-12)));
    return bestThing
}

katas13()

function katas14(){
    displayResults('Katas 14', JSON.stringify(bestThing.slice(23,38)));
    return bestThing
}

katas14()

function katas15(){
    displayResults('Katas 15', JSON.stringify(bestThing.substring(69)));
    return bestThing
}

katas15()

function katas16(){
    displayResults('Katas 16', JSON.stringify(bestThing.substring(23, 38)));
    return bestThing
}

katas16()

function katas17(){
    displayResults('Katas 17', JSON.stringify(bestThing.indexOf('only')));
    return bestThing
}

katas17()

function katas18(){
    displayResults('Katas 18', JSON.stringify(bestThing.indexOf('bit')));
    return bestThing
}

katas18()

function katas19(){
    let citiesarray = gotCitiesCSV.split(",")
    let resultsarray = []
    let doublevowels = /aa|ee|ii|oo|uu/g
    for(let cities of citiesarray) {
        if (doublevowels.test(cities)) {
            resultsarray.push(cities)
        }
    }
    displayResults('Katas 19', JSON.stringify(resultsarray));
    return gotCitiesCSV
}

katas19()

function find(arr,query){
    return arr.filter(function(el){
        return el.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    })
}

function katas20(){
    let solution = find(lotrCitiesArray, 'or')
    displayResults('Katas 20', JSON.stringify(solution));
    return lotrCitiesArray
}

katas20()

function katas21(){
   let bestThingArray = bestThing.split(" ")
   let solution = find(bestThingArray, 'b')
   displayResults('Katas 21', JSON.stringify(solution));
   return bestThingArray
}

katas21()

function katas22(){
    if(lotrCitiesArray.includes('Mirkwood') === true){
        displayResults('Katas 22', 'Yes');
        return lotrCitiesArray
    } else {
        displayResults('Katas 22', 'No')
        return lotrCitiesArray
    }
}

katas22()

function katas23(){
    if (lotrCitiesArray.includes('Hollywood') === true){
        displayResults('Katas 23', 'Yes');
        return lotrCitiesArray
    } else {
        displayResults('Katas 23', 'No');
        return lotrCitiesArray
    }
}

katas23()

function katas24(){
    let result = lotrCitiesArray.findIndex(lotrCitiesArray => lotrCitiesArray === "Mirkwood")
    displayResults('Katas 24', JSON.stringify(result))
    return lotrCitiesArray
}

katas24()

function katas25(){
    let check = ' '
    let result = []
    for (let city of lotrCitiesArray){
        if (city.includes(check) === true) {
            result.push(city)
        }
    }
    displayResults('Katas 25', JSON.stringify(result))
    return lotrCitiesArray
}

katas25()

function katas26() {
    displayResults('Katas 26', JSON.stringify(lotrCitiesArray.reverse()))
}

katas26()

function katas27() {
    displayResults('Katas 27', JSON.stringify(lotrCitiesArray.sort()))
}

katas27()

function katas28(){
    let charNum = []
    lotrCitiesArray.sort(function(a, b) {
        return a.length - b.length
        })
        displayResults('Katas 28', JSON.stringify(lotrCitiesArray))
    }
    katas28()

function katas29(){
    lotrCitiesArray.pop()
    displayResults('Katas 29', JSON.stringify(lotrCitiesArray))
}

katas29()

function katas30(){
    lotrCitiesArray.push('Deadest Marshes')
    displayResults('Katas 30', JSON.stringify(lotrCitiesArray))
}

katas30()

function katas31(){
    lotrCitiesArray.shift()
    displayResults('Katas 31', JSON.stringify(lotrCitiesArray))
}

katas31()

function katas32() {
    lotrCitiesArray.unshift('Rohan')
    displayResults(('Katas 32'), JSON.stringify(lotrCitiesArray))
}

katas32()